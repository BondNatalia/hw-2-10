
const ul = document.querySelector(".tabs");
ul.addEventListener("click", (event) => {
 let activeLi = document.querySelector(".active");
 activeLi.classList.remove("active");
 event.target.classList.add("active");
 activeLi = document.querySelector(".active");
 showContent(activeLi);
});

function showContent(selectedLi) {
const tabsContent = document.querySelector(".tabs-content");
const content = tabsContent.children;
for(const child of content) {
 child.classList.add("unvisible");

 if(child.dataset.title === selectedLi.textContent) {

  child.classList.remove("unvisible");
 }

}
}



